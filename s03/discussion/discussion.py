# [SECTION] Functions
# Functions are blocks of code that run when called.
# SYNTAX: def function_name(parameters): expression.

def my_greeting():
    print("Hello user!");

# Calling/Invoking a function
print("---------------------------------------------------------");
print("Result of Calling/Invoking a function:");
my_greeting();

def greet_user(username):
    print(f"Hello, {username}!");

greet_user("Nicah");

print("---------------------------------------------------------");
# Return statements
def add_numbers(a, b):
    return a + b;

result = add_numbers(5, 10);

print("Result of add_numbers function:");
print(f"The sum is {result}");

# [SECTION]
# Lambda functions
# SYNTAX: lambda parameter: expression
print("---------------------------------------------------------");
print("Lambda functions:");

greeting = lambda person : f"Hello, {person}";
print(greeting("Rylie"));
print(greeting("Emil"));

# SYNTAX: lambda parameter: return value
multiplication = lambda a, b, : a * b;

print(multiplication(3, 5));
print(multiplication(30, 50));

print("---------------------------------------------------------");
print("Result of Mini-activity:");
squared_num = lambda num : num * num;
print(f"5 squared is {squared_num(5)}.");


# [SECTION] Classes
# Classes would serve as blueprints to describe the concept of objects. 
# Each object has characteristics (properties) and behaviors (methods).
print("---------------------------------------------------------");
print("Classes:");

class Car():
    # To initialize class properties, we need to define __init__() method
	# self refers to the current instance of the class
    def __init__(self, brand, model, year, color, mileage):
        self.brand = brand;
        self.model = model;
        self.year = year;
        self.color = color;
        self.mileage = mileage;

        # Other properties can be added and assigned hard-coded values
        self.fuel = "Gasoline";
        self.fuel_level = 0;
        self.distance = 0;

    # Methods
    def fill_fuel(self):
        print(f"Current fuel level: {self.fuel_level}");
        print("Filling up the fuel tank...");
        self.fuel_level = 100;
        print(f"New fuel level: {self.fuel_level}");

    def drive(self, distance):
        self.distance = distance;
        self.fuel_level = self.fuel_level - self.distance;
        print(f"The car has driven {self.distance} kilometers.");
        print(f"The fuel level left: {self.fuel_level}");

# Creating a new instance of class
new_car = Car("Nissan", "Kicks", "2023", "Gray", 0);

print("Result of Displaying properties:");
print(new_car.brand);
print(new_car.model);
print(new_car.year);

print("Result of Calling method of an instance:")
new_car.fill_fuel();
new_car.drive(50);

# There are four main fundamental principles in OOP
	# Encapsulation
	# Inheritance
	# Polymorphism
	# Abstraction
# Mechanism of wrapping the attributes and code acting on the methods together as a single unit.
# Attributes of a class will be hidden from other classes (data hiding).

class Person():
    def __init__(self):
        # By adding "_" in the attribute, we are declaring that it is protected or hidden from other classes.
        self._name = "John Doe";
        self._age = 30;

    # setter method is used to modify the value of a private or protected attribute.
    def set_name(self, name): self._name = name;

    # getter method is used to retrieve the value of a private or protected attribute.
    def get_name(self):
        print(f"Name of the Person: {self._name}");

    def get_name_val(self): return self._name;

    def set_age(self, age): self._age = age;
    
    def get_age(self): 
        print(f"Age of the Person: {self._age}");

person1 = Person();

print("---------------------------------------------------------");
print("ENCAPSULATION");
print(f"Name of the Person: {person1.get_name_val()}");

person1.get_name();
person1.set_name("Jane Doe");
person1.get_name();

person1.get_age();
person1.set_age(25);
person1.get_age();

# [SECTION]
# Inheritance
# The transfer of the characteristics of a parent class to child classes that are derived from it.

# SYNTAX: class ChildClassName(ParentClassName):
class Employee(Person):
    def __init__(self, employeeId):
        # super() can be use to access attributes and methods defined in the parent class
        super().__init__();
        self._employeeId = employeeId;

    def get_employeeId(self):
        print(f"The employee ID is {self._employeeId}");

    def set__employeeId(self, employeeId):
        self._employeeId = employeeId;

    # custom method
    def get_details(self):
        print(f"{self._employeeId} belongs to {self._name}");

employee1 = Employee("Emp-001");
employee1.get_details();

print("---------------------------------------------------------");
print("INHERITANCE");
employee1.set_name("Bob Doe");
employee1.set_age(28);
employee1.get_name();
employee1.get_age();
employee1.get_details();

class Student(Person):
    def __init__(self, studentNo, course, year_level):
        super().__init__();
        self._studentNo = studentNo;
        self._course = course;
        self._year_level = year_level;

    def get_details(self):
        print(f"{self._name} is currently in year {self._year_level} taking up {self._course}.");

    def set_studentNo(self, studentNo): self._studentNo = studentNo;
    def set_course(self, course): self._course = course;
    def set_year_level(self, year_level): self._year_level = year_level;

    def get_studentNo(self): print(f"Student No: {self._studentNo}");
    def get_course(self): print(f"Course: {self._course}");
    def get_year_level(self): print(f"Year Level: {self._year_level}");

print("---------------------------------------------------------");
student1 = Student("std-001", "IT", 1);
student1.set_name("Alice Smith");
student1.set_age(28);
student1.get_name();
student1.get_age();
student1.get_details();

# Polymorphism
# A child class inherits all the methods from the parent class. 
# However, in some situations, the method inherited from the parent class doesn’t quite fit into the child class. 
# In such cases, you will have to re-implement methods in the child class.
class Zuitt():
    def tracks(self):
        print("We are currently offering 3 tracks (Developer Career, Pi-shaped Career, and Short Courses)");

    def num_of_hours(self):
        print("Learn web development in 360 hours!");

print("POLYMORPHISM:");
zuitt = Zuitt();
zuitt.tracks();
zuitt.num_of_hours();

class DeveloperCareer(Zuitt):
    def num_of_hours(self):
        print("Learn the basics of web development in 240 hours!");

print("---------------------------------------------------------");
course1 = DeveloperCareer();
course1.num_of_hours();

class PiShapedCareer(Zuitt):
	def num_of_hours(self):
		# override the parent's num_of_hours() method
		print("Learn skills for no-code app development in 140 hours!");

course2 = PiShapedCareer();
course2.num_of_hours();

class ShortCourses(Zuitt):
	def num_of_hours(self):
		# override the parent's num_of_hours() method
		print("Learn advanced topics in web development in 20 hours!");

course3 = ShortCourses();
course3.num_of_hours();
print("---------------------------------------------------------");

# Abstraction
# An abstract class can be considered as a blueprint for other classes. 
# It allows you to create a set of methods that must be created within any child classes built from the abstract class.
# Abstract classes are used to provide a common interface for different implementations for different classes.

# abc is a built-in module in Python 
# "ABC" provides the base for defining Abstract Base Classes (ABCs). 
# “abstractmethod” is used to declare abstract method.
from abc import ABC, abstractmethod

class Polygon(ABC):
    # @abstractmethod - creates an abstract method that needs to be implemented by classes that inherits "Polygon" class
    @abstractmethod
    def printNumberOfSides(self): pass;
    # "pass" statement is a null operation that does nothing when it is executed

class Triangle(Polygon):
    def __init__(self):
        super().__init__();

    def printNumberOfSides(self):
        print("This polygon have 3 sides.");

print("ABSTRACTION:");
shape1 = Triangle();
shape1.printNumberOfSides(); # Can't instantiate abstract class Triangle w/o an implementation of abstract method.

class Pentagon(Polygon):
    def __init__(self):
        super().__init__();

    def printNumberOfSides(self):
        print("This polygon have 5 sides.");

shape2 = Pentagon();
shape2.printNumberOfSides();


