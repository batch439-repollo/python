name = "Ronald";
age = 30;
occupation = "Software Engineer";
movie = "KungFu Panda 4";
rating = 95.5;

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%.");

num1 = 10;
num2 = 15;
num3 = 12;

print(num1 * num2);
print(num1 < num3);
print(num2 + num3);
