# To execute a python program:
# 1. windows - python discussion.py
# 2. mac - python3 discussion.py

# print() is a function that prints the contents of the function to the terminal.
print("Hello World!");

# [SECTION] Variables and data types
# Data types convey what kind of information a variable holds.

# Strings (str) - for alphanumeric characters and symbols.
full_name = "John Doe";
secret_code = 'Pa$$w0rd';
print(full_name);

# Numbers (int, float, complex) - for integer, decimals, complex.
age = 25;
pi_approx = 3.1416;
complex_num = 1 + 5j;

# Boolean (bool) - for truth values.
is_learning = True; isLearning = True;
is_difficult = False; isDifficult = False;

# Constant variables are typically declared by using UPPERCASE letters for identifier name.
# INTEREST = 3.5

# Assigning values to multiple variables
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo";
print("-------------------------------------------------------------------------");
print("Result of assigning values to multiple variables:");
print(name1);
print(name2);
print(name3);
print(name4);
print(name1, name2, name3, name4);

print("-------------------------------------------------------------------------");
my_name = "Ronald Allan Repollo";
print("My name is " + my_name);
# Python can only concatenate strings.

# Concatenation with number data type
# print("My age is " + age); - TypeError: can only concatenate str (not "int") to str
# [SECTION] Typecasting
# Typecasting is the process of converting an object from one data type to another.

print("-------------------------------------------------------------------------");
print("Result of TypeCasting:");
# str() converts the value to strings.
print("My age is " + str(age));

# int() converts the value to integer.
print(int(pi_approx));

# float() converts the value to decimal.
print(float(age));

# complex() converts the value to complex value
complex_number = complex(3, 4);
print(complex_number);

# Another way to avoid type error in printing w/o the use of typecasting is the use of  F-strings.
print("Result of Using F-strings:");
print(f"My age is {age}");

# [SECTION] Operators
# Arithmetic Operators - performs mathematical operations
print("-------------------------------------------------------------------------");
print("Result of Arithmetic Operators:");

# Addition
print(1 + 10);

# Subtraction
print(15 - 8);

# Multiplication
print(18 * 9);

# Division
print(21 / 7);

# Modulo (Remainder)
print(18 % 4);

# Exponentiation
print(2 ** 6);

# Assignment operators - used to assign values to variables.
print("-------------------------------------------------------------------------");
print("Result of Assignment Operators:");

num1 = 3;
print(num1);

num1 += 4;
print(num1);

# Other assignment operators: -=, *=, /=, %=

# [SECTION]
# Comparison operators - used to compare values (retuns a boolean value)
print("-------------------------------------------------------------------------");
print("Result of Comparison Operators:");
print(1 == 1);
print("1" == 1);

# Other comparison operators: !=, >, <, >=, <=

# Logical Operators - used to combine conditional statements.
print("-------------------------------------------------------------------------");
print("Result of Logical Operators:");

# is_learning = True; isLearning = True;
# is_difficult = False; isDifficult = False;
and_operator = isLearning and isDifficult;
print(f"AND opertor: {and_operator}"); print(and_operator);

or_operator = isLearning or isDifficult;
print(f"OR opertor: {or_operator}"); print(or_operator);

not_operator = not isDifficult;
print(f"NOT operator: {not_operator}"); print(not_operator);

combined_logical_operator = isLearning and not isDifficult;
print(f"Combined logic operator: {combined_logical_operator}");

# [SECTION] User Input
# input() method allows users to provide input to the program.
print("-------------------------------------------------------------------------");
username = input("Please enter your username:\n");
print(f"Hello, {username}! Welcome to the Python Short Course!");

# In Python, the input method always return the user's input as a string.
num1 = int(input("Enter 1st number:\n"));
num2 = int(input("Enter 2nd number:\n"));
print(f"The sum of num1 and num2 is {num1 + num2}");
#print(f"The sum of num1 and num2 is {int(num1) + int(num2)}");

# [SECTION] Selection Control Structures
# Selection control structures are used to execute different blocks of code based on certain condition.
print("-------------------------------------------------------------------------");

# if else statements
passing_score = 100;

if passing_score >= 75:
    print("Test passed!");
else:
    print("Test failed!");

# If-elif-else statements
# elif is shorthand for else if.
test_integer = int(input("Please enter a number:\n"));

if test_integer > 0:
    print("The number is positive.");
elif test_integer == 0:
    print("The number is zero.");
else:
    print("The number is negative.");

print("-------------------------------------------------------------------------");
print("Mini-activity:")

test_div_num = int(input("Please enter a number:\n"));

if test_div_num % 3 == 0 and test_div_num % 5 == 0 and test_div_num != 0:
    print("The number is divisible by both 3 and 5");
elif test_div_num % 3 == 0 and test_div_num != 0:
    print("The number is divisible by 3");
elif test_div_num % 5 == 0 and test_div_num != 0:
    print("The number is divisible by 5");
else:
    print("The number is not divisible by either 3 or 5");
