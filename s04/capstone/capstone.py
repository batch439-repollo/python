# Capstone Specifications:
# 1. Create a "Person" class that is an abstract class that has the following methods:
	# a. getFullName method
	# b. addRequest method
	# c. checkRequest method
	# d. addUser method

# 2. Create an "Employee" class from Person with the following properties and methods:
	# a. Properties
		# i. firstName
		# ii. lastName
		# iii. email
		# iv. department
	# b. Getters and Setters Method
	# c. Abstract methods
		# i. getFullName method - returns "<First name Last Name>"
		# ii. addRequest method - returns "Request has been added"
		# iii. checkRequest method - pass
		# iv. addUser method - pass
	# d. Custom methods
		# i. login() - returns "<Email> has logged in"
		# ii. logout() - returns "<Email> has logged out"

# 3. Create a "TeamLead" class from Person with the following properties and methods:
	# a. Properties
		# i. firstName
		# ii. lastName
		# iii. email
		# iv. department
		# v. members (empty list and no need to add getter and setter)
	# b. Getters and Setters Method
	# c. Abstract methods
		# i. getFullName method - returns "<First name Last Name>"
		# ii. addRequest method - pass
		# iii. checkRequest method - returns "Request has been checked"
		# iv. addUser method - pass
	# d. Custom methods
		# i. login() - returns "<Email> has logged in"
		# ii. logout() - returns "<Email> has logged out"
		# iii. addMember() - adds a team member

# 4. Create an "Admin" class from Person with the following properties and methods
	# a. Properties(make sure they are private and have getters/setters)
		# i. firstName
		# ii. lastName
		# iii. email
		# iv. department
	# b. Getters and Setters Method
	# c. Abstract methods
		# i. getFullName method - returns "<First name Last Name>"
		# ii. addRequest method - pass
		# iii. checkRequest method - pass
		# iv. addUser method - returns "User has been added"
	# d. Custom methods
		# i. login() - returns "<Email> has logged in"
		# ii. logout() - returns "<Email> has logged out"

# 5. Create a "Request" class that has the following properties and methods:
	# a. properties
		# i. name
		# ii. requester
		# iii. dateRequested
		# iv. status
	# b. Getters and Setters Method
	# c. Custom methods
		# i. updateRequest() - returns "<name> has been updated"
		# ii. closeRequest() - returns "<name> has been closed"
		# iii. cancelRequest() - returns "<name> has been cancelled"

# Add your solutions here:
# 1.
from abc import ABC, abstractmethod

class Person(ABC):
    @abstractmethod
    def getFullName(self): pass;

    @abstractmethod
    def addRequest(self): pass;

    @abstractmethod
    def checkRequest(self): pass;

    @abstractmethod
    def addUser(self): pass;

# 2.
class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__();
        self.firstName = firstName;
        self.lastName = lastName;
        self.email = email;
        self.department = department;

    # Getters and Setters:
    def get_first_name(self): return self.firstName;
    def set_first_name(self, firstName): self.firstName = firstName;

    def get_last_name(self): return self.lastName;
    def set_last_name(self, lastName): self.lastName = lastName;

    def get_email(self): return self.email;
    def set_email(self, email): self.email = email;

    def get_department(self): return self.department;
    def set_department(self, department): self.department = department;

    # Abstract Methods:
    def getFullName(self): return f"{self.firstName} {self.lastName}";
    def addRequest(self): return "Request has been added";
    def checkRequest(self): pass;
    def addUser(self): pass;

    # Custom Methods:
    def login(self): return f"{self.email} has logged in";
    def logout(self): return f"{self.email} has logged out";

# 3.
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__();
        self.firstName = firstName;
        self.lastName = lastName;
        self.email = email;
        self.department = department;
        self.members = [];

    # Getters and Setters:
    def get_first_name(self): return self.firstName;
    def set_first_name(self, firstName): self.firstName = firstName;

    def get_last_name(self): return self.lastName;
    def set_last_name(self, lastName): self.lastName = lastName;

    def get_email(self): return self.email;
    def set_email(self, email): self.email = email;

    def get_department(self): return self.department;
    def set_department(self, department): self.department = department;

    def get_members(self): return self.members;
    def set_members(self, members): self.members = members;

    # Abstract Methods:
    def getFullName(self): return f"{self.firstName} {self.lastName}";
    def addRequest(self): pass;
    def checkRequest(self): return f"Request has been checked";
    def addUser(self): pass;

    # Custom Methods:
    def login(self): return f"{self.email} has logged in";
    def logout(self): return f"{self.email} has logged out";
    def addMember(self, employee): self.members.append(employee);

# 4.
class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__();
        self.firstName = firstName;
        self.lastName = lastName;
        self.email = email;
        self.department = department;

    # Getters and Setters:
    def get_first_name(self): return self.firstName;
    def set_first_name(self, firstName): self.firstName = firstName;

    def get_last_name(self): return self.lastName;
    def set_last_name(self, lastName): self.lastName = lastName;

    def get_email(self): return self.email;
    def set_email(self, email): self.email = email;

    def get_department(self): return self.department;
    def set_department(self, department): self.department = department;

    # Abstract Methods:
    def getFullName(self): return f"{self.firstName} {self.lastName}";
    def addRequest(self): pass;
    def checkRequest(self): pass;
    def addUser(self): return "User has been added";

    # Custom Methods:
    def login(self): return f"{self.email} has logged in";
    def logout(self): return f"{self.email} has logged out";

# 5.
class Request():
    def __init__(self, name, requester, dateRequested):
        super().__init__();
        self.name = name;
        self.requester = requester;
        self.dateRequested = dateRequested;
        self.status = "";

    # Getters and Setters:
    def get_name(self): return self.name;
    def set_name(self, name): self.name = name;

    def get_requester(self): return self.requester;
    def set_requester(self, requester): self.requester = requester;

    def get_dateRequested(self): return self.dateRequested;
    def set_dateRequested(self, dateRequested): self.dateRequested = dateRequested;

    def get_status(self): return self.status;
    def set_status(self, status): self.status = status;

    # Custom Methods:
    def updateRequest(self): return f"{self.name} has been updated";
    def closeRequest(self): return f"{self.name} has been closed";
    def cancelRequest(self): return f"{self.name} has been cancelled";


# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing");
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing");
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales");
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales");
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing");
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales");
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021");
req2 = Request("Laptop repair", employee1, "1-Jul-2021");

assert employee1.getFullName() == "John Doe", "Full name should be John Doe";
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin";
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter";
assert employee2.login() == "sjane@mail.com has logged in";
assert employee2.addRequest() == "Request has been added";
assert employee2.logout() == "sjane@mail.com has logged out";

teamLead1.addMember(employee3);
teamLead1.addMember(employee4);
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName());

assert admin1.addUser() == "User has been added";

req2.set_status("closed");
print(req2.closeRequest());