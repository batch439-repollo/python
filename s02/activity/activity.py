# 1.
try:
    year = int(input("Please input a year:\n"));
    
    if year <= 0:
        print("Zero and negative values are not allowed, please enter a valid year.");
    elif (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        print(year, "is a leap year.");
    else:
        print(year, "is not a leap year.");
except ValueError:
    print("Strings are not allowed, please enter a valid year.");


# 2.
print("---------------------------------------------------------\n");
rows = int(input("Enter number of rows:\n"));
columns = int(input("Enter number of columns:\n"));

for i in range(rows):
    for j in range(columns):
        print('*', end='');
    print();

