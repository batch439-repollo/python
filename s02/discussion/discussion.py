# [SECTION] Loops
# Repetition control structures aka. as loops allow you to execute a block of code repeatedly.

# While Loops
# A while loop will continue to execute a block of code as long as the given condition is true.
print("-----------------------------------------------------------");
print("Result of while loop:");
i = 1;

while i <= 5:
    print(f"Current count: {i}");
    i += 1;

# For Loops
print("-----------------------------------------------------------");
print("Result of for loop:");
fruits = ["apple", "banana", "cherry"];

for fruit in fruits:
    print(fruit);

# range() function
# range() function in Python is a built-in function used to generate a sequence of numbers.
print("-----------------------------------------------------------");
print("Result of for loop using range:");

# SYNTAX: for x in range(stop) - sequence will start at 0 and ends at stop-1.
for i in range(6):
    print(f"The current value is: {i}");

# SYNTAX: for x in range(start, stop) - sequence will start at 10 and end at stop-1.
print("\n")
for i in range(10, 16):
    print(f"The current value is: {i}");

print("\n")
for i in range(10, 0, -1):
    print(f"The current value is: {i}");

# Break Statement
# The break statement is used to stop the loop.
print("-----------------------------------------------------------");
print("Result of break statement:");

j = 1;
while j < 6:
    print(j);

    if j == 3:
        break;
    
    j += 1;

# Continue Statement
# The continue statement is used to skip the rest of the code inside a loop for the current iteration and proceed t the next iteration of the loop.
print("-----------------------------------------------------------");
print("Result of Continue statement:");

k = 1;
while k < 6:
    k += 1;

    if k == 3:
        continue;

    print(k);

# Placing a continue statement before iteration variable could lead to an infinite loop.
# k = 1;

# while k < 6:
	
# 	if k == 3:
# 		continue

# 	print(k);

# 	k += 1;

# [SECTION] Lists
# Lists are similar to arrays in JS in a sense that they can contain a collection of data.
print("-----------------------------------------------------------");
print("Result of Lists:");

names = ["John", "Paul", "George", "Ringo"];
programs = ['developer career', 'pi-shape', 'main course package'];
durations = [260, 180, 20];
truth_values = [True, False, True, True, False];

# List w/ different data types.
sample_list = ["Apple", 3, 'Potato', True];

# Getting the list size
# The number of elements in a list can be counted using the len() method.
print(len(programs));

# Accesing values
# Lists can be accessed by providing the index number of the element.
print("Result of Accesing values:");
print(programs[0]);
print(programs[-1]);

# Mini-activity
print("-----------------------------------------------------------");
print("Result of Mini-activity:");
print(durations[1]);

# Accessing a range of values
# Lists can be sliced to access a range of values.
# SYNTAX: [start index:end index]
# Note that the end index is not included
print("-----------------------------------------------------------");
print("Result of Accessing a range of value:");
print(programs[0:2])

# Updating lists
# Lists can be updated by assigning a new value to an existing index.
print("-----------------------------------------------------------");
print(f"Current value: {programs[2]}");
programs[2] = 'short course';
print(f"Updated value: {programs[2]}");

# [SECTION] List Manipulation
# Adding list items
print("-----------------------------------------------------------");
print("Result of Adding list items:");
programs.append("global");
print(programs);

# Deleting list items
print("-----------------------------------------------------------");
print("Result of Deleting list items:");
del durations[-1];
print(durations);

# Membership checks - return true or false
print("-----------------------------------------------------------");
print("Result of IN operator:");
print("John" in names);
print(20 in durations);

# Sorting lists
print("-----------------------------------------------------------");
print("Result of Sorting lists:");

names.sort();
print(names);

names.sort(reverse=True);
print(names);

# Emptying the list
print("-----------------------------------------------------------");
print("Result of Emptying the list:");
print(durations);
durations.clear();
print(durations); # durations emptied

# [SECTION] Dictionaries
# Dictionaries are used to store data values in key:value pairs.
# This is similar to objects in JS.
print("-----------------------------------------------------------");
print("Dictionaries:");

person = {
    "name": "John",
    "age": 30,
    "occupation": "student",
    "isEnrolled": True,
    "subjects": ["MySQL", "Python", "Django"],
    "city": "New York",
    "country": "USA"
}

# Getting the number of key-pairs in the dictionary
print("-----------------------------------------------------------");
print("Result of getting the number of key-pairs in the dictionary:");
print(len(person));

# Accessing values in dictionary
print("-----------------------------------------------------------");
print("Result of Accessing values in the dictionary:");
print(person["name"]);

# Getting all the keys in the dictionary
print("Result of getting all the keys in the dictionary:")
print(person.keys());

# Getting all the values in the dictionary
print("Result of getting all the values in the dictionary:")
print(person.values());

# Getting all the key value pairs in the dictionary
print("Result of Getting all the key value pairs in the dictionary:");
print(person.items());

# Adding key value pairs
print("-----------------------------------------------------------");
print("Result of update() method:");

person.update({"city": "Manila"});
person.update({"fave_food": "sushi"});
print(person.items());

# Deleting key value pairs
print("Result of pop() method:");
person.pop("fave_food");
print(person.items());

# Emptying the dictionary
person1 = {
    "name": "John",
    "age": 20
}

print(person1);
print("Result of clear() method:");
person1.clear();
print(person1);

# Looping through dictionaries
# {key} represents the current key being iterated over
# {person1[key]} represents the value associated with the key in the dictionary person1
print("-----------------------------------------------------------");
for key in person:
    print(f"The value of {key} is {person[key]}.");

# Nested dictionaries
print("-----------------------------------------------------------");

person2 = {
	"name": "Nicah",
	"age": 20,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["MySQL", "Python", "Django"]	
};

batch349 = {
    "student1": person2,
    "student2": {
	    "name": "Nicah",
	    "age": 20,
	    "occupation": "student",
	    "isEnrolled": True,
	    "subjects": ["MySQL", "Python", "Django"]	
    }
}

print("Result of nested dictionaries:");
print(batch349);
